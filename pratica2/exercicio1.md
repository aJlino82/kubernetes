# Prática 2 #

<!-- TOC -->

- [Prática 2](#prática-2)
- [Objetivos](#objetivos)
- [Volumes](#volumes)
  - [Empty-Dir](#empty-dir)
  - [Persistent Volume](#persistent-volume)
- [Secrets](#secrets)
- [ConfigMaps](#configmaps)

<!-- TOC -->

# Objetivos

Os objetivos deste exercício são:

* Mostrar como gerenciar volumes, secrets e configmaps;

------------------------------

> Alguns comandos foram extraídos/adaptados do livro Descomplicando Kubernetes: https://github.com/badtuxx/DescomplicandoKubernetes

# Volumes

## Empty-Dir

Um volume do tipo **EmptyDir** é criado sempre que um Pod é atribuído a um node existente. Esse volume é criado inicialmente vazio, e todos os containers do Pod podem ler e gravar arquivos no volume.

**Esse volume não é um volume com persistência de dados**. Sempre que o Pod é removido de um nó, os dados no ``EmptyDir`` são excluídos permanentemente. É importante ressaltar que os dados não são excluídos em casos de falhas nos containers.

Crie o arquivo ``pod-emptydir.yaml`` com o seguinte conteúdo para criar um pod que utilizará esse tipo de volume.

```yaml
apiVersion: v1
kind: Pod
metadata:
  name: busybox
spec:
  containers:
  - image: busybox
    name: busy
    command:
      - sleep
      - "3600"
    volumeMounts:
    - mountPath: /mnt
      name: curso-k8s-dir
  volumes:
  - name: curso-k8s-dir
    emptyDir: {}
```

Crie o volume a partir do manifesto.

```bash
kubectl create -f pod-emptydir.yaml -n NAMESPACE
```

Visualize os pods.

```bash
kubectl get pod -n NAMESPACE
```

Liste o nome do conteiner que está dentro do pod ``busybox``:

```bash
kubectl get pods busybox -o jsonpath='{.spec.containers[*].name}*' -n NAMESPACE
```

Agora adicione um arquivo dentro do path ``/mnt`` diretamente no pod criado:

```bash
kubectl exec -ti busybox -c busy -n NAMESPACE -- touch /mnt/funciona
```

Agora liste esse diretório:

```bash
kubectl exec -ti busybox -c busy -n NAMESPACE -- ls -l /mnt
```

O arquivo foi criado corretamente?

Acesse o shell do conteiner ``busy``, que está dentro do pod ``busybox``:

```bash
kubectl exec -ti busybox -c busy -n NAMESPACE -- sh
```

Liste o conteúdo do diretório /mnt.

```bash
ls /mnt
```

Remova o pod.

```bash
kubectl delete -f pod-emptydir.yaml -n NAMESPACE
```

## Persistent Volume

> Por estarmos utilizando o kind para simular um cluster k8s, não será fácil manipular um volume persistente. Nesse caso, é recomendado você estudar e praticar este tempo por conta própria seguindo as instruções da página: https://github.com/badtuxx/DescomplicandoKubernetes/blob/master/day-4/DescomplicandoKubernetes-Day4.md#persistent-volume

# Secrets

Crie o arquivo ``secret.txt`` com o seguinte comando.

```bash
echo -n "curso-k8s" > secret.txt
```

Agora crie um recurso do tipo ``Secret``.

```bash
kubectl create secret generic my-secret --from-file=secret.txt -n NAMESPACE
```

Veja os detalher desse recurso:

```bash
kubectl describe secret my-secret -n NAMESPACE
```

> Observe que não é possível ver o conteúdo do arquivo utilizando o ``describe``, isso é para proteger a chave de ser exposta acidentalmente.

Para verificar o conteúdo de um ``Secret``, é necessário decodificar o arquivo gerado em **base64**. Mas antes obtenha o conteúdo da secret ``my-secret``.

```bash
kubectl get secret my-secret -o yaml -n NAMESPACE
```

O conteúdo do arquivo ``secret.txt`` é exibido na linha:

```yaml
data:
  secret.txt: Y3Vyc28tazhz
```

Agora decodifique o conteúdo **Base64** com o seguinte comando:

```bash
echo 'Y3Vyc28tazhz' | base64 --decode
```

Agora utilize a secret dentro de um Pod. Para isso, crie o arquivo ``pod-secret.yaml`` com o seguinte conteúdo.

Informe o seguinte conteúdo.

```yaml
apiVersion: v1
kind: Pod
metadata:
  name: teste-secret
spec:
  containers:
  - image: busybox
    name: busy
    command:
      - sleep
      - "3600"
    volumeMounts:
    - mountPath: /mnt
      name: my-volume-secret
  volumes:
  - name: my-volume-secret
    secret:
      secretName: my-secret
```

Nesse manifesto está sendo utilizado o volume ``my-volume-secret`` para ser montar a secret ``my-secret`` no diretório ``/mnt``.

```bash
kubectl create -f pod-secret.yaml -n NAMESPACE
```

Verifique se a ``Secret`` foi criada corretamente.

```bash
kubectl exec -ti teste-secret -n NAMESPACE -- ls /mnt

kubectl exec -ti teste-secret -n NAMESPACE -- cat /mnt/secret.txt
```

Deu certo? Entendeu o processo? Existe um jeito ainda mais bacana que é utilizando as secrets como variável de ambiente.

Crie um novo recurso do tipo ``Secret`` usando chave e valor.

```bash
kubectl create secret generic my-literal-secret --from-literal user=aecio --from-literal password=pires -n NAMESPACE
```

Veja os detalhes do objeto ``my-literal-secret``:

```bash
kubectl describe secret my-literal-secret -n NAMESPACE
```

Percebeu que foi criado um recurso do tipo ``Secret`` com duas chaves: ``user`` e ``password``?

Referencie essa chave dentro do pod utilizando variável de ambiente, para isso crie o arquivo ``pod-secret-env.yaml`` com o seguinte conteúdo.

```yaml
apiVersion: v1
kind: Pod
metadata:
  name: teste-secret-env
spec:
  containers:
  - image: busybox
    name: busy-secret-env
    command:
      - sleep
      - "3600"
    env:
    - name: MEU_USERNAME
      valueFrom:
        secretKeyRef:
          name: my-literal-secret
          key: user
    - name: MEU_PASSWORD
      valueFrom:
        secretKeyRef:
          name: my-literal-secret
          key: password
```

Crie o pod:

```bash
kubectl create -f pod-secret-env.yaml -n NAMESPACE
```

Agora liste as variáveis de ambiente dentro do container para verificar se a Secret realmente foi criada.

```bash
kubectl exec -it teste-secret-env -c busy-secret-env -n NAMESPACE -- printenv | sort
```

Funcionou? Foram criadas as variáveis de ambiente ``MEU_PASSWORD`` e ``MEU_PASSWORD``?

Remova os pods e secrets recém criados.

```bash
kubectl delete -f pod-secret.yaml -n NAMESPACE
kubectl delete -f pod-secret-env.yaml -n NAMESPACE
```

# ConfigMaps

Crie um diretório chamado ``vingadores`` e nele adicione alguns personagens e suas características com os seguintes comandos.

```bash
mkdir vingadores

echo "Se acha o último biscoito do pacote." > vingadores/homem-de-ferro

echo "Esse é duro de matar e passaram formol nele." > vingadores/capitao-america

echo "É um pirralho" > vingadores/homem-aranha

echo "É mais forte que todos os homens juntos" > vingadores/capita-marwell

echo "Esse é realmente um filme de tirar o folego e cheio de contextualização com a realidade preconceituoza" > vingadores/pantera-negra
```

Crie o ``Configmap``.

```bash
kubectl create configmap nomes-vingadores --from-literal=jaspion=nao-eh-um-vingador --from-literal=giraya=tataravo-dos-power-rangers --from-file=vingadores/ -n NAMESPACE
```

Visualize o Configmap.

```bash
kubectl get configmap -n NAMESPACE
```

Crie o arquivo ``pod-configmap.yaml`` com o seguinte conteúdo:

```yaml
apiVersion: v1
kind: Pod
metadata:
  name: busybox-configmap
spec:
  containers:
  - image: busybox
    name: busy-configmap
    command:
      - sleep
      - "3600"
    env:
    - name: GIRAYA
      valueFrom:
        configMapKeyRef:
          name: nomes-vingadores
          key: giraya
    - name: JASPION
      valueFrom:
        configMapKeyRef:
          name: nomes-vingadores
          key: jaspion
```

Crie o pod a partir do manifesto.

```bash
kubectl create -f pod-configmap.yaml -n NAMESPACE
```

Agora liste as variáveis de ambiente dentro do container para verificar se o ConfigMap Secret realmente foi criado.

```bash
kubectl exec -it busybox-configmap -c busy-configmap -n NAMESPACE -- printenv | sort
```

Funcionou? Foram criadas as variáveis de ambiente ``GIRAYA`` e ``JASPION``?

Agora crie o arquivo ``pod-configmap-file.yaml`` com o seguinte conteúdo:

```yaml
apiVersion: v1
kind: Pod
metadata:
  name: busybox-configmap-file
spec:
  containers:
  - image: busybox
    name: busy-configmap
    command:
      - sleep
      - "3600"
    volumeMounts:
    - name: meu-configmap-vol
      mountPath: /etc/vingadores
  volumes:
  - name: meu-configmap-vol
    configMap:
      name: nomes-vingadores
```

Crie o pod a partir do manifesto.

```bash
kubectl create -f pod-configmap-file.yaml -n NAMESPACE
```

Verifique se o ``ConfigMap`` foi criado corretamente.

```bash
kubectl exec -ti busybox-configmap-file -n NAMESPACE -- ls -l /etc/vingadores/

kubectl exec -ti busybox-configmap-file -n NAMESPACE -- sh -c "cat /etc/vingadores/capita-marwell && cat /etc/vingadores/giraya && cat /etc/vingadores/homem-de-ferro && cat /etc/vingadores/pantera-negra && cat /etc/vingadores/capitao-america && cat /etc/vingadores/homem-aranha && cat /etc/vingadores/jaspion"
```

Remova os pods e configmaps recém criados.

```bash
kubectl delete -f pod-configmap.yaml -n NAMESPACE
kubectl delete -f pod-configmap-file.yaml -n NAMESPACE
```