
# Prática 3 #

<!-- TOC -->

- [Prática 3](#prática-3)
- [Objetivos](#objetivos)
- [Criando um service ClusterIP](#criando-um-service-clusterip)
- [Criando um service NodePort](#criando-um-service-nodeport)
- [Criando um service LoadBalancer](#criando-um-service-loadbalancer)
  - [EndPoint](#endpoint)
- [Rollouts e Rollbacks](#rollouts-e-rollbacks)

<!-- TOC -->

# Objetivos

Os objetivos deste exercício são:

* Apresentar os tipos de services no cluster Kubernetes;
* Mostrar como fazer rollout  e rollback de pods;


------------------------------

> Alguns comandos foram extraídos/adaptados do livro Descomplicando Kubernetes: https://github.com/badtuxx/DescomplicandoKubernetes

# Criando um service ClusterIP

Crie o arquivo ``service-clusterip.yaml`` com as seguintes definições a serem utilizadas para criar um service do tipo **ClusterIP**:

```yaml
apiVersion: v1
kind: Service
metadata:
  labels:
    run: nginx
  name: nginx-clusterip
spec:
  ports:
  - port: 80
    protocol: TCP
    targetPort: 80
  selector:
    run: nginx
  type: ClusterIP
```

Crie o service.

```bash
kubectl create -f service-clusterip.yaml -n NAMESPACE
```

Obtenha informações do service:

```bash
kubectl get services nginx-clusterip -n NAMESPACE
```

Visualize os detalhes do service.

```bash
kubectl describe service nginx-clusterip -n NAMESPACE
```

Remova o service.

```bash
kubectl delete -f service-clusterip.yaml -n NAMESPACE
```

# Criando um service NodePort

Crie o arquivo ``service-nodeport.yaml`` com as seguintes definições a serem utilizadas para criar um service do tipo **NodePort**:

```yaml
apiVersion: v1
kind: Service
metadata:
  labels:
    run: nginx
  name: nginx-nodeport
spec:
  externalTrafficPolicy: Cluster
  ports:
  - nodePort: 31111
    port: 80
    protocol: TCP
    targetPort: 80
  selector:
    run: nginx
  sessionAffinity: None
  type: NodePort
```

Crie o service.

```bash
kubectl create -f service-nodeport.yaml -n NAMESPACE
```

Obtenha informações do service.

```bash
kubectl get svc nginx-nodeport -n NAMESPACE
```

Visualize os detalhes do service.

```bash
kubectl describe service nginx-nodeport -n NAMESPACE
```

Remova o service.

```bash
kubectl delete -f service-nodeport.yaml -n NAMESPACE
```

# Criando um service LoadBalancer

Crie o arquivo ``service-loadbalancer.yaml`` com as seguintes definições a serem utilizadas para criar um service do tipo **LoadBalancer**:

```yaml
apiVersion: v1
kind: Service
metadata:
  labels:
    run: nginx
  name: nginx-loadbalancer
spec:
  externalTrafficPolicy: Cluster
  ports:
  - nodePort: 31222
    port: 80
    protocol: TCP
    targetPort: 80
  selector:
    run: nginx
  sessionAffinity: None
  type: LoadBalancer
```

Crie o service.

```bash
kubectl create -f service-loadbalancer.yaml -n NAMESPACE
```

Obtenha informações do service.

```bash
kubectl get svc nginx-loadbalancer -n NAMESPACE
```

> OBS.: O service do tipo LoadBalancer só funciona se você estiver utilizando o k8s a partir de um serviço gerenciado de um cloud provider. Exemplo: EKS-AWS, GKE-GCP, AKS-Azure, DKS-Digital Ocean. Em ambientes on-premisse há o [MetalLB](https://metallb.universe.tf) que pode ser instalado e configurado para atender a essa necessidade.

Visualize os detalhes do service.

```bash
kubectl describe service nginx-loadbalancer -n NAMESPACE
```

Removendo o service.

```bash
kubectl delete -f service-loadbalancer.yaml -n NAMESPACE
```

## EndPoint

Sempre que criamos um service, automaticamente é criado um endpoint. O endpoint nada mais é do que o IP do pod que o service irá utilizar, por exemplo, quando criamos um service do tipo ClusterIP temos o seu IP, correto?

Agora, quando batemos nesse IP ele redireciona a conexão para o Pod através desse IP, o EndPoint.

Para listar os EndPoints criados, execute o comando:

```bash
kubectl get endpoints
kubectl get endpoints -n NAMESPACE
```

Obtenha os detalhes do endpoint criado no namespace **default**:

```bash
kubectl describe endpoints kubernetes
```

Crie um deployment para o nginx.

```bash
kubectl create deployment nginx --image=nginx -n NAMESPACE
```

List o deployment.

```bash
kubectl get deployments.apps -n NAMESPACE
```

Escale o deployment para 3 réplicas do nginx.

```bash
kubectl scale deployment nginx --replicas=3 -n NAMESPACE
```

List novamente o deployment.

```bash
kubectl get deployments.apps -n NAMESPACE
```

Exponha o deployment nginx.

```bash
kubectl expose deployment nginx --port=80 -n NAMESPACE
```

Visualize o service.

```bash
kubectl get svc -n NAMESPACE
```

Visualize os endpoints.

```
kubectl get endpoints -n NAMESPACE
```

Visualize os detalhes do endpoint nginx.

```bash
kubectl describe endpoints nginx -n NAMESPACE
```

Remova o deployment e o service nginx.

```bash
kubectl delete deployment nginx -n NAMESPACE
kubectl delete service nginx -n NAMESPACE
```

# Rollouts e Rollbacks

Crie um deployment para o nginx.

```bash
kubectl create deployment nginx --image=nginx -n NAMESPACE
```

Visualize as informações do rollout e a revision do deployment recém criado.

```bash
kubectl rollout history deployment nginx -n NAMESPACE
```

Será mostrada a revision 1 e a informação de que não houve uma causa relacionada a mudança no versionamento do deployment, isso porque o deployment acabou de ser criado.

Visualize os detalhes da revision 1 do deployment:

```bash
kubectl rollout history deployment nginx --revision=1 -n NAMESPACE
```

Agora altere a versão da imagem do nginx.

```bash
kubectl edit deployment nginx -n NAMESPACE
```

Altere a seguinte linha:

Antes:

```yaml
...
      - image: nginx
...
```

Depois:

```yaml
...
      - image: nginx:1.15.0
...
```

Visualize novamente as informações do rollout.

```bash
kubectl rollout history deployment nginx -n NAMESPACE
```

Percebeu que foi adicionada a revision 2?

Visualize os detalhes da revision 2 do deployment:

```bash
kubectl rollout history deployment nginx --revision=2 -n NAMESPACE
```

Visualize os eventos do namespace.

```bash
kubectl get events -n NAMESPACE
```

Liste os pods:

```bash
kubectl get pods -n NAMESPACE
```

Visualize os detalhes de um dos pods:

```bash
kubectl describe pod POD_NAME -n NAMESPACE
```

Para voltar para a ``revision`` desejada do ``deployment nginx``, basta executar o seguinte comando:

```bash
kubectl rollout undo deployment nginx --to-revision=1 -n NAMESPACE
```

> Perceba que trocamos o ``history`` por ``undo`` e o ``revision`` por ``to-revision``, assim faremos o **rollback** em Deployment, e voltamos a versão da imagem que desejamos. 😃

Liste os pods:

```bash
kubectl get pods -n NAMESPACE
```

Visualize os detalhes de um dos pods:

```bash
kubectl describe pod POD_NAME -n NAMESPACE
```

Visualize novamente as informações do rollout.

```bash
kubectl rollout history deployment nginx -n NAMESPACE
```

Percebeu que foi adicionada a revision 3?

Visualize os detalhes da revision 3 do deployment:

```bash
kubectl rollout history deployment nginx --revision=3 -n NAMESPACE
```

Notou alguma semelhança com a revision 1?

---

Obs.: Por padrão, os recursos no Kubernetes persistem apenas as 10 últimas revisions. Se precisar aumentar este número, edite o manifesto YAML de um recurso e altere o valor do parâmetro ``revisionHistoryLimit``.
Fonte: https://kubernetes.io/docs/concepts/workloads/controllers/deployment/#clean-up-policy

---

Para acompanhar o status do rollout de um recurso execute o seguinte comando:

```
kubectl rollout status resource RESOURCE_NAME -n NAMESPACE
```

Exemplo:

```bash
kubectl rollout status deployment nginx -n NAMESPACE
```

Remova o deployment.

```bash
kubectl delete deployment nginx -n NAMESPACE
```