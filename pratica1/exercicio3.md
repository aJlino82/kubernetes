# Prática 1 #

<!-- TOC -->

- [Prática 1](#prática-1)
- [Objetivos](#objetivos)
  - [Kubectl Edit](#kubectl-edit)
- [ReplicaSet](#replicaset)
- [DaemonSet](#daemonset)
- [Cron Jobs](#cron-jobs)

<!-- TOC -->

# Objetivos

Os objetivos deste exercício são:

* Mostrar como editar a configuração de recursos em tempo de execução;
* Mostrar como manipular os objetos ReplicaSet;
* Mostrar como manipular os objetos DaemonSet;
* Apresentar como manipular os objetos Jobs e CronJobs;


------------------------------

> Alguns comandos foram extraídos/adaptados do livro Descomplicando Kubernetes: https://github.com/badtuxx/DescomplicandoKubernetes

## Kubectl Edit

Crie novamente o deployment limitando a utilização dos recursos.

```bash
kubectl create -f deployment-limitado.yaml -n NAMESPACE
```

Agora utilizar o comando ``kubectl edit`` para editar o deployment com o pod ainda em execução.

```bash
kubectl edit deployment nginx -n NAMESPACE
```

Abriu um editor, correto? Vamos alterar a especificação ``spec.containers.resources.limits.memory`` de **256Mi** para **512**.

O conteúdo a ser alterado é o seguinte:

Antes:

```yaml
...
    spec:
      containers:
      - image: nginx
        imagePullPolicy: Always
        name: nginx
        resources:
          limits:
            cpu: 200m
            memory: 256Mi
...
```

Depois:

```yaml
...
    spec:
      containers:
      - image: nginx
        imagePullPolicy: Always
        name: nginx
        resources:
          limits:
            cpu: 200m
            memory: 512Mi
...
```

Veja se o resultado foi aplicado conforme esperado:

```bash
kubectl get pods -n NAMESPACE
kubectl describe pod POD_NAME -n NAMESPACE
```

Percebeu que alguns pods foram encerrados e outros sendo criados? Isso é Kubernetes fazendo o rollout dos pods (é como se tivessa fazendo o deploy de uma nova versão). Ele aplica em um pod. Se o pod ficar estável (ou seja **Ready**), ele continua aplicando a alteração nos demais. Do contrário ele cancela o rollout e faz o rollback para a configuração anterior do deployment.

E o limit de memória do pod mudou?

Remova o deployment.

```bash
kubectl delete -f deployment-limitado.yaml -n NAMESPACE
```

# ReplicaSet

Crie o arquivo ``replicaset.yaml`` com o seguinte conteúdo:

```yaml
apiVersion: apps/v1
kind: ReplicaSet
metadata:
  name: replica-set
spec:
  replicas: 3
  selector:
    matchLabels:
      system: App
  template:
    metadata:
      labels:
        system: App
    spec:
      containers:
      - name: nginx
        image: nginx:1.7.9
        ports:
        - containerPort: 80
```

Crie o ReplicaSet a partir do manifesto YAML:

```bash
kubectl create -f replicaset.yaml -n NAMESPACE
```

Visualize o ReplicaSet:

```bash
kubectl get replicaset -n NAMESPACE
```

Obtenha mais informações do ReplicaSet:

```bash
kubectl describe rs replica-set -n NAMESPACE
```

Liste os pods em execução:

```bash
kubectl get pods -n NAMESPACE
```

Quanto pods tem em execução?

O que acontece se excluir dos pods?

```bash
kubectl delete pod POD_NAME -n NAMESPACE
```

Liste novamente os pods em execução:

```bash
kubectl get pods -n NAMESPACE
```

Edite o ReplicaSet e altere para 4 réplicas.

```bash
kubectl edit rs replica-set -n NAMESPACE
```

Antes:

```yaml
...
spec:
  replicas: 3
...
```

Depois:

```yaml
...
spec:
  replicas: 4
...
```

Liste novamente os pods em execução:

```bash
kubectl get pods -n NAMESPACE
```

Remova o ReplicaSet:

```bash
kubectl delete -f replicaset.yaml -n NAMESPACE
```

# DaemonSet

**DaemonSet** é basicamente a mesma coisa do que o ReplicaSet, com a diferença que quando você utiliza o DaemonSet você não especifica o número de réplicas, ele executará um pod por node em seu cluster.

Crie o arquivo ``daemonset.yaml`` com o seguinte conteúdo:

```yaml
apiVersion: apps/v1
kind: DaemonSet
metadata:
  name: daemon-set
spec:
  selector:
    matchLabels:
      system: MyApp
  template:
    metadata:
      labels:
        system: MyApp
    spec:
      containers:
      - name: nginx
        image: nginx:1.7.9
        ports:
        - containerPort: 80
```

Crie o DaemonSet:

```bash
kubectl create -f daemonset.yaml -n NAMESPACE
```

Liste os DaemonSet:

```bash
kubectl get daemonset -n NAMESPACE
```

Visualize os detalhes do DaemonSet:

```bash
kubectl describe ds daemon-set -n NAMESPACE
```

Visualizando os detalhes dos pods:

```bash
kubectl get pods -o wide -n NAMESPACE
```

Observe que tem um pod por node executando o ``daemon-set``.

Remova o DaemonSet:

```bash
kubectl delete -f daemonset.yaml -n NAMESPACE
```

# Cron Jobs

Um serviço **CronJob** nada mais é do que uma linha de um arquivo crontab o mesmo arquivo de uma tabela ``cron``. Ele agenda e executa tarefas periodicamente em um determinado cronograma. As "Cron" são úteis para criar tarefas periódicas e recorrentes, como executar backups ou enviar e-mails.

Cria o arquivo ``cron.yaml`` com o seguinte conteúdo.

```yaml
apiVersion: batch/v1beta1
kind: CronJob
metadata:
  name: curso-k8s
spec:
  schedule: "*/1 * * * *"
  jobTemplate:
    spec:
      template:
        spec:
          containers:
          - name: curso-k8s-cron
            image: busybox
            args:
            - /bin/sh
            - -c
            - date; echo Está gostando do curso de Kubernetes???????????? Diz que sim, vai! ;sleep 30
          restartPolicy: OnFailure
```

O objetivo desse cron é imprimir a hora atual e uma mensagem a cada minuto.

Crie um recurso do tipo ``CronJob`` a partir do manifesto.

```bash
kubectl create -f cron.yaml -n NAMESPACE
```

Agora liste o ``Cronjob``.

```bash
kubectl get cronjobs -n NAMESPACE
```

Visualize os detalhes do ``Cronjob``.

```bash
kubectl describe cronjobs.batch curso-k8s -n NAMESPACE
```

Agora veja se o ``cron`` está realmente funcionando através do comando a seguir.

```bash
kubectl get jobs --watch -n NAMESPACE
```

Visualize o CronJob.

```bash
kubectl get cronjob  curso-k8s -n NAMESPACE
```

O cron está está funcionando corretamente? Para visualizar a saída dos comandos executados pela tarefa, utilize os comandos a seguir:

```bash
kubectl get pods -n NAMESPACE
kubectl logs POD_NAME -n NAMESPACE
```

O ``cron`` deve estar executando corretamente as tarefas de imprimir a data e a frase que criamos no manifesto.

> OBS.: Por padrão, o Kubernetes mantém o histórico dos últimos 3 ``cron`` executados, concluídos ou com falhas.
> Fonte: https://kubernetes.io/docs/tasks/job/automated-tasks-with-cron-jobs/#jobs-history-limits

Remova o CronJob.

```bash
kubectl delete -f cron.yaml -n NAMESPACE
```
