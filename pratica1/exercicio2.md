# Prática 1 #

<!-- TOC -->

- [Prática 1](#prática-1)
- [Objetivos](#objetivos)
- [Limitando Recursos](#limitando-recursos)

<!-- TOC -->

# Objetivos

Os objetivos deste exercício são:

* Mostrar como limitar os recurso dos pods;

------------------------------

> Alguns comandos foram extraídos/adaptados do livro Descomplicando Kubernetes: https://github.com/badtuxx/DescomplicandoKubernetes

# Limitando Recursos

Quando criamos um Pod podemos especificar a quantidade de CPU e Memória (RAM) que pode ser consumida em cada container. Quando algum container contém a configuração de limite de recursos o Scheduler fica responsável por alocar esse container no melhor nó possível de acordo com os recursos disponíveis.

Podemos configurar dois tipos de recursos, CPU que é especificada em **unidades de núcleos** e Memória que é especificada em **unidades de bytes**.

Vamos criar nosso primeiro Deployment com limite de recursos, para isso vamos subir a imagem de um nginx e copiar o yaml do deployment com o seguinte comando.

```bash
kubectl create deployment nginx --image=nginx -n NAMESPACE
```

Escale o deployment para 3 réplicas.

```bash
kubectl scale deployment nginx --replicas=3  -n NAMESPACE
```

Obtenha a lista de deployments.

```bash
kubectl get deployments -n NAMESPACE
```

Visualize os detalhes do deployment:

```bash
kubectl describe deployment nginx -n NAMESPACE
```

Visualize os pods:

```bash
kubectl get pods -n NAMESPACE
```

Visualize os detalhes de um pod:

```bash
kubectl describe pod/POD_NAME -n NAMESPACE
```

Remover o deployment do nginx.

```bash
kubectl delete deployments.apps nginx -n NAMESPACE
```

Crie o arquivo ``deployment-limitado.yaml`` com o seguinte conteúdo.

```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  labels:
    app: nginx
  name: nginx
spec:
  progressDeadlineSeconds: 600
  replicas: 3
  revisionHistoryLimit: 10
  selector:
    matchLabels:
      app: nginx
  strategy:
    rollingUpdate:
      maxSurge: 25%
      maxUnavailable: 25%
    type: RollingUpdate
  template:
    metadata:
      creationTimestamp: null
      labels:
        app: nginx
    spec:
      containers:
      - image: nginx
        imagePullPolicy: Always
        name: nginx
        resources:
          limits:
            memory: "256Mi"
            cpu: "200m"
          requests:
            memory: "128Mi"
            cpu: "50m"
        terminationMessagePath: /dev/termination-log
        terminationMessagePolicy: File
      dnsPolicy: ClusterFirst
      restartPolicy: Always
      schedulerName: default-scheduler
      securityContext: {}
      terminationGracePeriodSeconds: 30
```

> Atenção! **1 core de CPU** corresponde a ``1000m`` (1000 milicore). Ao especificar ``200m``, estamos querendo reservar 20% de 1 core da CPU. Se fosse informado o valor ``0.2`` teria o mesmo efeito, ou seja, seria reservado 20% de 1 core da CPU.

Agora crie o deployment limitando a utilização dos recursos.

```bash
kubectl create -f deployment-limitado.yaml -n NAMESPACE
```

Obtenha a lista de deployments.

```bash
kubectl get deployments -n NAMESPACE
```

Visualize os detalhes do deployment:

```bash
kubectl describe deployment nginx -n NAMESPACE
```

Percebeu alguma diferença em relação ao deployment anterior? Qual?

Visualize os pods:

```bash
kubectl get pods -n NAMESPACE
```

Visualize os detalhes de um pod:

```bash
kubectl describe pod/POD_NAME -n NAMESPACE
```

Percebeu alguma diferença em relação ao pod anterior? Qual?

Vamos acessar um container e testar a configuração.

```bash
kubectl exec -ti POD_NAME  -n NAMESPACE -- /bin/bash
```

Dentro do container, instale e execute o ``stress`` para simular a carga em nossos recursos, no caso CPU e memória.

Instalando o comando stress.

```bash
apt-get update && apt-get install -y stress
```

Executando o ``stress``.

```bash
stress --vm 1 --vm-bytes 128M --cpu 1

stress: info: [221] dispatching hogs: 1 cpu, 0 io, 1 vm, 0 hdd
```

Aqui estamos _stressando_ o container, utilizando 128M de RAM e 1 core de CPU. Brinque de acordo com os limites que você estabeleceu.

Teste outras combinações de stress dos recursos:

```bash
stress --vm 1 --vm-bytes 512M --cpu 1
stress --vm 1 --vm-bytes 1024M --cpu 1
```

Saia do pod e visualize o log do mesmo.

```bash
exit
kubectl logs POD_NAME -n NAMESPACE
```

Observe que no log apareceu mensagens de erro semelhantes a seguir: 

```log
2020/08/15 15:38:09 [alert] 1#1: unknown process 313 exited on signal 2
2020/08/15 15:38:09 [alert] 1#1: unknown process 314 exited on signal 2
2020/08/15 15:39:35 [alert] 1#1: unknown process 316 exited on signal 2
```

Essas mensagens de erros ocorreram porque o processo tentou utilizar mais recursos de CPU e memória do que era permitido para o pod (lembra dos limites definidos no arquivo ``deployment-limitado.yaml``?) Como o processo não pode ultrapassa o limite configurado, o mesmo foi encerrado.

Remova o deployment.

```bash
kubectl delete -f deployment-limitado.yaml -n NAMESPACE
```