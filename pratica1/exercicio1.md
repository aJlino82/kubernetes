# Prática 1 #

<!-- TOC -->

- [Prática 1](#prática-1)
- [Objetivos](#objetivos)
- [VirtualBox](#virtualbox)
- [Google Cloud](#google-cloud)
- [Playground Kubernetes](#playground-kubernetes)
- [Kubectl](#kubectl)
- [Helm](#helm)
- [Autenticando no Cluster Kubernetes](#autenticando-no-cluster-kubernetes)
  - [Virtualbox](#virtualbox-1)
  - [Na GCP](#na-gcp)
  - [No Playgroud Kubernetes](#no-playgroud-kubernetes)
- [Gerenciando um cluster k8s](#gerenciando-um-cluster-k8s)

<!-- TOC -->

# Objetivos

Os objetivos deste exercício são:

* Mostrar como acessar um cluster Kubernetes;
* Apresentar alguns comandos mais usados do Kubernetes;
* Mostrar como criar e listar os pods;
* Criar namespaces;
* Especificar configurações dos pods utilizando arquivos YAML;
* Criar Services;
* Acessar os últimos eventos do cluster;
* Obter informações detalhadas a cerca dos pods, nodes e services;
* Obter a ajuda do comando ``kubectl``.

------------------------------

# VirtualBox

Se estiver usando uma máquina virtual instalada e configurada conforme as instruções previamente enviadas pelo professor, ligue a máquina virtual e passe para a seção [Autenticando no Cluster Kubernetes](#autenticando-no-cluster-kubernetes).

> OBS.: Os seguintes softwares já estão instalados e configurados na máquina virtual: ``kubeclt``, ``krew``, ``kubectx``, ``kubens``, ``kubefw``, ``docker-ce``, ``helm`` e ``kind``.

# Google Cloud

Se estiver usando o Google Cloud para praticar os exercícios, crie um cluster Kubernetes no GKE seguindo as instruções das páginas a seguir. Orientação mais precisas serão passadas pelo professor.

* https://medium.com/@rcorrea/gke-criando-meu-primeiro-cluster-no-google-kubernetes-engine-parte-1-3446e562e5cd
* https://cloud.google.com/kubernetes-engine/docs/how-to/private-clusters?hl=pt-br

> OBS.: Preferenciamente utilize a máquina virtual compartilhada pelo professor.

Ao término da configuração passe para a seção [Kubectl](#kubectl).

> OBS.: Por estar utilizando o Kubernetes gerenciado no Google Cloud, não é necessário instalar o ``docker-ce`` e ``kind``.

# Playground Kubernetes

Se estiver usando o site [Play with Kubernetes](https://labs.play-with-k8s.com) para praticar os exercícios, faça login com a gratuita, previamente crida no [Docker Hub](https://hub.docker.com), e passe para a seção [Autenticando no Cluster Kubernetes](#autenticando-no-cluster-kubernetes).

> OBS.: Por estar utilizando o Playground Kubernetes, não é necessário instalar o ``kubectl`` e ``docker-ce``.

# Kubectl

Na sua estação de trabalho/estudo, instale o ``kubectl``, utilizando as instruções da página a seguir de acordo com o sistema operacional.

* https://kubernetes.io/docs/tasks/tools/install-kubectl/

# Helm

Na sua estação de trabalho/estudo, instale o ``helm``, utilizando as instruções da página a seguir de acordo com o sistema operacional.

* https://helm.sh/docs/intro/install/

Após a instalação, passe para a seção [Autenticando no Cluster Kubernetes](#autenticando-no-cluster-kubernetes).

# Autenticando no Cluster Kubernetes

## Virtualbox

Acesse via SSH a máquina virtual compartilhada pelo professor.

Para testar o acesso ao cluster, visualize o status dos nodes e pods em no cluster com os seguintes comandos.

```bash
kubectl get nodes
kubectl get pods --all-namespaces
```

## Na GCP

Considerando que você já instalou e configurou o comando ``gcloud`` durante a instalação do cluster na GCP, acesse o console da GCP em: https://console.cloud.google.com

Ao lado do nome **Google Cloud Platform** (na barra de títulos), selecione o nome do projeto que possui o cluster configurado. Veja um exemplo a seguir.

![alt text](images/gcp-selecionando_projeto.png "Selecionando um projeto na GCP")

* No menu principal da GCP, selecione a opção **Kubernetes Engine** e, em seguida, clique na opção **Clusters**.

* Localize o cluster que precisa atuar e clique no botão **Connect** para obter o comando que configura o ``gcloud`` para acessar o cluster.

* Copie e execute o comando no terminal.

Para testar o acesso, visualize o status dos nodes e pods em no cluster com os seguintes comandos.

```bash
kubectl get nodes
kubectl get pods --all-namespaces
```

> Repita este procedimento para conectar ao cluster Kubernetes de cada projeto na GCP.

## No Playgroud Kubernetes

Adicione uma instância.

Execute o seguinte comando para inicializar um nó master na instância.

```bash
kubeadm init --apiserver-advertise-address $(hostname -i) --pod-network-cidr 10.5.0.0/16
```

> OBS.: Esse é o mesmo comando informado no console da instância recém criada no Playground Kubernetes.

Ao final do comando, será exibida uma mensagem semelhante a mostrada a seguir contendo um comando para adicionar um node no cluster.

```bash
hen you can join any number of worker nodes by running the following on each as root:

kubeadm join 192.168.0.21:6443 --token 3uwx3x.fjb5vutojh56i41v \
    --discovery-token-ca-cert-hash sha256:aad16149bb2a84390ff39244ab6ad49ec74a74c175ca5ac7b598aa1fd4f14f2a
```

Adicione uma nova instância e execute o comando semelhante ao mostrado anteriormente.

De volta a instância do **master**, execute o seguinte comando para inicializar a rede no cluster.

```bash
kubectl apply -f https://raw.githubusercontent.com/cloudnativelabs/kube-router/master/daemonset/kubeadm-kuberouter.yaml
```

Para testar o acesso, visualize o status dos nodes no cluster com o seguinte comando.

```bash
kubectl get nodes
```

> Os nodes estarão prontos quando o status na segunda coluna do resultado a seguir for **ready**. Isso pode demorar alguns minutos.

```bash
NAME    STATUS     ROLES    AGE     VERSION
node1   NotReady   master   4m48s   v1.18.4
node2   NotReady   <none>   53s     v1.18.4
```

Verifique o status dos pods em no cluster com o seguinte comando.

```bash
kubectl get pods --all-namespaces
```

Verifique o nome do cluster ao qual está conectado:

```bash
kubectl config current-context
kubectl config view
kubectl cluster-info
```

Para autenticar em outro cluster Kubernetes consulte a documentação do comando a seguir:

```bash
kubectl config --help
```

> Opcionalmente acesse as seguintes páginas para obter mais informações para acessar um cluster remoto:
> 
> * https://kubernetes.io/docs/tasks/access-application-cluster/configure-access-multiple-clusters/
> * https://blog.christianposta.com/kubernetes/logging-into-a-kubernetes-cluster-with-kubectl/

Para visualizar novamente o token para inserção de novos *nodes*, execute o seguinte comando.

```bash
kubeadm token create --print-join-command
```

# Gerenciando um cluster k8s

> Alguns comandos foram extraídos/adaptados do livro Descomplicando Kubernetes: https://github.com/badtuxx/DescomplicandoKubernetes

Execute o comando a seguir para exibir informações detalhadas sobre determinado node do cluster.

```bash
kubectl describe node NODE
```

Visualize os namespaces:

```bash
kubectl get namespaces
```

Visualize os pods de um determinado namespace:

```bash
kubectl get pods -n NAMESPACE
```

Visualize todos os pods de todos os namespaces:

```bash
kubectl get pods --all-namespaces
```

Visualize todos os pods de todos os namespaces utilizando a opção ```-o wide```, que disponibiliza maiores informações sobre o recurso:

```bash
kubectl get pods --all-namespaces -o wide
```

Crie um namespace com o seu nome.

```bash
kubectl create namespace NAMESPACE
```

Inicie um pod em um namespace específico no k8s com o seguinte comando:

```bash
kubectl run hellok8s --image scottsbaldwin/docker-hello-world:latest -n NAMESPACE
```

> Se você esquecer de utilizar a opção ``-n NAMESPACE`` no ``kubectl``, o namespace padrão será o **default**. Em ambientes de produção, evite utilizar o namespace **default** para manter uma organização dos recursos e facilitar a aplicação de permissões e limites de CPU e memória por namespace.

Visualize o pod recém criado no seu namespace.

```bash
kubectl get pods -n NAMESPACE
```

Visualize mais informações sobre o pod.

```bash
kubectl describe pod/hellok8s -n NAMESPACE
```

Verifique quais foram os últimos eventos do cluster:

```bash
kubectl get events
```

Exporte a configuração do pod recém criado para um arquivo **YAML**.

```bash
kubectl get pod/hellok8s -n NAMESPACE -o yaml > hellok8s.yaml
```

Será criado um arquivo chamado ``hellok8s.yaml``. Visualize o conteúdo do arquivo recém criado.

Observando o arquivo anterior, notamos que este reflete o **estado** do *pod*. Nós desejamos utilizar tal arquivo apenas como um modelo, e sendo assim, podemos apagar as entradas que armazenam dados de estado desse *pod*, como *status* e todas as demais configurações que são específicas dele. O arquivo final ficará com o conteúdo semelhante a este:

```yaml
apiVersion: v1
kind: Pod
metadata:
  creationTimestamp: null
  labels:
    run: hellok8s
  name: hellok8s
spec:
  containers:
  - image: scottsbaldwin/docker-hello-world:latest
    name: hellok8s
    resources: {}
  dnsPolicy: ClusterFirst
  restartPolicy: Always
status: {}
```

Remova o pod com o seguinte comando.

```bash
kubectl delete pod hellok8s -n NAMESPACE
```

Recrie o pod a partir do arquivo ``hellok8s.yaml``.

```bash
kubectl create -f hellok8s.yaml -n NAMESPACE
```

Observe que não foi necessário informar ao ``kubectl`` qual tipo de recurso seria criado, pois isso já está contido dentro do arquivo.

Liste o pod novamente.

```bash
kubectl describe pod/hellok8s -n NAMESPACE
```

Verifique novamente os últimos eventos do cluster:

```bash
kubectl get events
```

Algo mudou? Por que?

Uma outra forma de criar um arquivo de *template* é através da opção ``--dry-run`` do ``kubectl``, com o funcionamento ligeiramente diferente dependendo do tipo de recurso que será criado. Exemplos:

Para a criação do template de um *pod*:

```bash
kubectl run hellok8s --image scottsbaldwin/docker-hello-world:latest -n NAMESPACE --dry-run=client -o yaml > pod-template.yaml
```

Verifique se há diferença entre os arquivos ``pod-template.yaml`` e ``hellok8s.yaml``.

```bash
diff pod-template.yaml hellok8s.yaml
```

A vantagem deste método é que serão apresentadas apenas as opções necessárias do recurso.

Sempre que precisar, obtenha ajuda do comando ``kubectl`` com a opção ```explain``` ou com ``--help``. Exemplos:

```bash
kubectl explain [recurso]
kubectl explain [recurso.caminho.para.spec]
kubectl explain [recurso.caminho.para.spec] --recursive
kubectl --help
kubectl <command> --help
```

Exemplos:

```bash
kubectl explain deployment
kubectl explain pod --recursive
kubectl explain deployment.spec.template.spec
kubectl --help
kubectl get --help
kubectl delete --help
kubectl run --help
kubectl exec --help
```

Computadores fora do *cluster*, por padrão, não conseguem acessar os *pods*. Primeiro é necessário expor o *pod* com o seguinte comando:

```bash
kubectl expose pod hellok8s -n NAMESPACE
```

Ops! Apareceu um erro, não foi?

    > error: couldn't find port via --port flag or introspection
    > See 'kubectl expose -h' for help and examples

O erro ocorreu devido ao fato do k8s não saber qual é a porta de destino do contêiner que deve ser exposta (no caso, a 80/TCP). Para configurá-la, é necessário remover o pod:

```bash
kubectl delete -f hellok8s.yaml -n NAMESPACE
```

Abra agora o arquivo ``hellok8s.yaml``. Altere o conteúdo para ficar semelhante ao mostrado a seguir.

```yaml
apiVersion: v1
kind: Pod
metadata:
  creationTimestamp: null
  labels:
    run: hellok8s
  name: hellok8s
spec:
  containers:
  - image: scottsbaldwin/docker-hello-world:latest
    ports:
      - containerPort: 80
    name: hellok8s
    resources: {}
  dnsPolicy: ClusterFirst
  restartPolicy: Always
status: {}
```

Basicamente foi adiciona a seção **ports** para especificar a porta padrão do conteiner.

> Atenção: arquivos **YAML** utilizam para sua tabulação dois espaços e não *tab*.

Agora crie o pod com o comando a seguir:

```bash
kubectl create -f hellok8s.yaml -n NAMESPACE
```

Liste o pod:

```bash
kubectl get pods -n NAMESPACE
```

Agora tente expor o pod novamente. O comando a seguir cria um objeto do k8s chamado de *Service*, que é utilizado justamente para expor *pods* para acesso externo.

```bash
kubectl expose pod POD_NAME -n NAMESPACE
```

Liste todos os *services* com o comando a seguir.

```bash
kubectl get services -n NAMESPACE
```

Por padrão, o service é do tipo **ClusterIP**, mostrado na coluna **TYPE**. Este tipo de service só expõe o pod dentro do cluster e utilizando o IP do cluster, mostrado na coluna **CLUSTER-IP**. 

> Quem está utilizando o k8s com o kind (instalação padrão na máquina virtual não conseguirá acessar o pod, porque não há uma interface de rede com endereço na mesma rede do cluster k8s).

Tente acessar a aplicação com o seguinte comando:

```bash
curl ip-cluster:80
```

Visualize mais detalhes do service recém criado.

```bash
kubectl describe service hellok8s -n NAMESPACE
```

Acesse o prompt de comandos do conteiner principal do pod.

```bash
kubectl exec -it  pod/hellok8s -n NAMESPACE -- sh
```

Obtenha a página html da aplicação do pod com os seguintes comandos.

```bash
wget localhost
cat index.html
```

Saia do pod com o seguinte comando:

```bash
exit
```

Visualize o log do pod com um dos seguintes comandos:

```bash
kubectl logs  pod/hellok8s -n NAMESPACE

ou

kubectl logs -f pod/hellok8s -n NAMESPACE
```

Para mostrar todos os recursos recém criados, pode-se utilizar uma das seguintes opções a seguir.

```bash
kubectl get all -n NAMESPACE
kubectl get pod,service -n NAMESPACE
kubectl get pod,svc -n NAMESPACE
```

Note que o k8s nos disponibiliza algumas abreviações de seus recursos. Com o tempo você irá se familiar com elas. Para apagar os recursos criados, você pode executar os seguintes comandos.

```bash
kubectl delete -f hellok8s.yaml -n NAMESPACE
kubectl delete service hellok8s -n NAMESPACE
```

Liste novamente os recursos para ver se os mesmos ainda estão presentes.
