<!-- TOC -->

- [Dicas para executar no fim de cada aula](#dicas-para-executar-no-fim-de-cada-aula)
  - [Google Cloud](#google-cloud)

<!-- TOC -->

# Dicas para executar no fim de cada aula

## Google Cloud

Algumas dicas para economizar e aumentar a segurança das instâncias no Google Cloud.

* Desligue as instâncias utilizadas na aula.

Procedimento para desligar as instâncias:

1) No Google Cloud (https://console.cloud.google.com), clique no menu **Compute engine** e escolha a opção **VM instances**.
2) Selecione as instâncias utilizadas na aula e clique no botão **STOP**.
