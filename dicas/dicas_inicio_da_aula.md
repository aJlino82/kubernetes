<!-- TOC -->

- [Dicas para executar no início de cada aula](#dicas-para-executar-no-início-de-cada-aula)
- [Google Cloud](#google-cloud)

<!-- TOC -->

# Dicas para executar no início de cada aula

Se estiver utilizando o Windows para acompanhar os exercícios, instale os seguintes softwares:

* Putty: https://www.chiark.greenend.org.uk/~sgtatham/putty/latest.html
* WinSCP: https://winscp.net/eng/download.php

Você pode usar o editor de texto de sua preferência e que se sinta mais confortável de usar. Mas o VSCode (https://code.visualstudio.com), combinado com os seguintes plugins, auxilia o processo de edição/revisão, principalmente permitindo a pré-visualização do conteúdo antes do commit, analisando a sintaxe do Markdown e gerando o sumário automático, à medida que os títulos das seções vão sendo criados/alterados.

* Markdown-lint: https://marketplace.visualstudio.com/items?itemName=DavidAnson.vscode-markdownlint
* Markdown-toc: https://marketplace.visualstudio.com/items?itemName=AlanWalk.markdown-toc
* Markdown-all-in-one: https://marketplace.visualstudio.com/items?itemName=yzhang.markdown-all-in-one
* Helm-intellisense: https://marketplace.visualstudio.com/items?utm_sq=ggv6n6jy52&itemName=Tim-Koehler.helm-intellisense

# Google Cloud

* Ligue as instâncias a serem utilizadas na aula.

Procedimento para ligar as instâncias:

1) No Google Cloud (https://console.cloud.google.com), clique no menu **Compute engine** e escolha a opção **VM instances**.
2) Selecione as instâncias utilizadas na aula e clique no botão **START**.