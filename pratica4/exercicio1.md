
# Prática 4 #

<!-- TOC -->

- [Prática 4](#prática-4)
- [Objetivos](#objetivos)
- [Helm](#helm)
  - [Comandos Básicos do Helm 3](#comandos-básicos-do-helm-3)
  - [Criando um Helm Chart](#criando-um-helm-chart)

<!-- TOC -->

# Objetivos

Os objetivos deste exercício são:

* Mostrar como gerenciar aplicações no k8s usando o Helm;
* Mostrar como criar, publicar, editar um helm chart;


------------------------------

> Alguns comandos foram extraídos/adaptados do livro Descomplicando Kubernetes: https://github.com/badtuxx/DescomplicandoKubernetes

# Helm

O **Helm** é o gerenciador de pacotes do Kubernetes. Os pacotes gerenciados pelo Helm, são chamados de **charts**, que basicamente são formados por um conjunto de manifestos Kubernetes no formato YAML e alguns templates que ajudam a manter variáveis dinâmicas de acordo com o ambiente. O Helm ajuda você a definir, instalar e atualizar até o aplicativo Kubernetes mais complexo.

Os Helm charts são fáceis de criar, versionar, compartilhar e publicar.

O Helm é um projeto graduado no CNCF e é mantido pela comunidade, assim como o Kubernetes.

Para obter mais informações sobre o Helm, acesse os seguintes links:

* https://helm.sh
* https://helm.sh/docs/intro/quickstart
* https://www.youtube.com/watch?v=Zzwq9FmZdsU&t=2s
* https://helm.sh/docs/topics/architecture 

---

## Comandos Básicos do Helm 3

Obtenha a ajuda do helm sempre que precisa com o comando:

```bash
helm --help
helm install --help
```

Adicione o repositório oficial de Helm charts estáveis:

```bash
helm repo add stable https://kubernetes-charts.storage.googleapis.com
```

Liste os repositórios Helm adicionados:

```bash
helm repo list
```

> OBS.: Para remover um repositório Helm, execute o comando: `helm repo remove NOME_REPOSITORIO`.

Obtenha a lista atualizada de Helm charts disponíveis para instalação utilizando todos os repositórios Helm adicionados. Seria o mesmo que executar o comando ``apt-get update``.

```bash
helm repo update
```

Liste quais os charts estão disponíveis para ser instalados:

```bash
helm search repo stable
```

> OBS.: O comando ``helm search repo`` pode ser utilizado para listar os charts em todos os repositórios adicionados.

Temos ainda a opção de listar os charts disponíveis a partir do Helm Hub (tipo o Docker Hub):

```bash
helm search hub
```

Visualize quais as 5 versões mais recentes do chart do Prometheus disponíveis para instalação:

```bash
helm search repo prometheus -l | head -n 6
```

* A coluna **NAME** mostra o nome do repositório e o chart.
* A coluna **CHART VERSION** mostra apenas a versão mais recente do chart disponível para instalação.
* A coluna **APP VERSION** mostra apenas a versão mais recente da aplicação a ser instalada pelo chart. Mas nem todo o time de desenvolvimento mantém a versão da aplicação atualizada no campo *APP VERSION*. Eles fazem isso para evitar gerar uma nova versão do chart só porque a aplicação mudou, sem haver mudanças na estrutura do chart. Dependendo de como o chart é desenvolvido, a versão da aplicação é alterada apenas no manifesto ``values.yaml`` que cita qual a imagem Docker que será instalada pelo chart.

Obtenha as informações do chart do Prometheus com o seguinte comando:

```bash
helm show chart stable/prometheus
```

Essas informações do chart segue a mesma ideia do comando ``docker inspect`` utilizado para inspecionar uma imagem ou um contêiner.

Obtenha a documentação de uso do chart disponível no arquivo ``README.md`` do chart do Prometheus com o seguinte comando:

```bash
helm show readme stable/prometheus
```

Obtenha a lista de parâmetros e valores padrão do chart do Prometheus com o seguinte comando:

```bash
helm show values stable/prometheus
```

Viu? O comando ``helm show values`` apresenta os valores e parâmetros padrão de um chart.

Exporte os parâmetros e valores padrão do chart do Prometheus para o arquivo ``prometheus_values.yaml``:

```bash
helm show values stable/prometheus > prometheus_values.yaml
```

Edite o arquivo ``prometheus_values.yaml`` e altere a seguinte linha:

Antes:

```bash
tag: v2.20.1
```

Depois:

```bash
tag: v2.20.0
```

Salve o arquivo. Propositadamente você alterou uma configuração simples: a versão da imagem Docker do Prometheus a ser instalada pelo Helm chart.

Instale o Prometheus:

```bash
helm install meu-prometheus -f prometheus_values.yaml --version=11.12.0 stable/prometheus -n NAMESPACE
```

> O Helm na vesão 3 não cria o namespace. É necessário criá-lo antes. 
>
> Não confunda a versão do Helm Chart com a versão da imagem Docker da aplicação.

Liste as aplicações instaladas com o Helm:

```bash
helm list -n NAMESPACE
```

Verifique o status dos pods:

```bash
kubectl get pods -n NAMESPACE
```

Visualize os detalhes do pod que tem o prefixo ``meu-prometheus-server``.

```bash
kubectl describe pods POD_NAME -n NAMESPACE
```

Percebeu que este pod tem mais de um contêiner? Ele utiliza o padrão sidecar.

Visualize o log do pod:

```bash
kubectl logs POD_NAME -n NAMESPACE
```

Ops! Ocorreu um erro, não foi? Como este pod tem mais de um contêiner, é necessário informar o nome do contêiner que você deseja olhar o log.

Exemplo:

```bash
kubectl logs POD_NAME -c prometheus-server -n NAMESPACE
```

Visualize novamente os detalhes do pod que tem o prefixo ``meu-prometheus-server``.

```bash
kubectl describe pods POD_NAME -n NAMESPACE
```

Edite novamente o arquivo ``prometheus_values.yaml`` e altere a seguinte linha:

Antes:

```bash
tag: v2.20.0
```

Depois:

```bash
tag: v2.20.1
```

Atualize o Prometheus:

```bash
helm upgrade meu-prometheus -f prometheus_values.yaml --version=11.12.0 stable/prometheus -n NAMESPACE
```

Verifique o status dos pods:

```bash
kubectl get pods -n NAMESPACE
```

Visualize novamente os detalhes do pod que tem o prefixo ``meu-prometheus-server``.

```bash
kubectl describe pods POD_NAME -n NAMESPACE
```

Podemos ver nas linhas ``Liveness`` e ``Readness`` que o Prometheus está sendo executado na porta 9090/TCP.

Crie um redirecionamento entre a porta 9090/TCP do pod e a porta 9091/TCP da computador local:

```bash
kubectl port-forward POD_NAME 9091:9090 --namespace NAMESPACE
```

Abra outra sessão SSH com a máquina virtual.

Agora digite o seguinte comando:

```bash
elinks http://127.0.0.1:9091
```

O que aconteceu? Tente explicar o motivo.

> OBS.: O comando ``kubectl port-forward`` cria um redicionamento do tráfego 9091/TCP do computador que tem o kubectl instalado para a porta 9090 do pod que está no cluster k8s. Saiba mais em: https://kubernetes.io/docs/tasks/access-application-cluster/port-forward-access-application-cluster/#forward-a-local-port-to-a-port-on-the-pod

Visualize os deployments:

```bash
kubectl get deployment -n NAMESPACE
```

Percebeu algo diferente? Foi o Helm quem criou todos os deployments adicionais necessário ao funcionamento do prometheus. Funciona como um gerenciado de dependências de pacotes.

Visualize os objetos replicaSet:

```bash
kubectl get replicaset -n NAMESPACE
```

Visualize o services:

```bash
kubectl get services -n NAMESPACE
```

O Helm também criou esses objetos no cluster ao instalar a aplicação ``meu-prometheus``.

Visualize a versão mais recente do chart do Grafana disponível para instalação:

```bash
helm search repo grafana
```

Use o chart do Grafana para instalar a aplicação ``meu-grafana`` com os valores padrão:

```bash
helm install meu-grafana --version=5.5.5 stable/grafana -n NAMESPACE
```

Se quiser visualizar quais foram os valores padrão utilizados para esta instalação, utilize o comando:

```bash
helm get values meu-grafana
```

> Este comando difere do comando ``helm show values`` porque mostra os valores com o qual uma aplicação helm foi implantada, mesmo que os valores sejam diferentes do padrão que está definido no chart. É muito útil para você entender com que valores uma aplicação helm foi implantada em produção.

Observe que logo após a instalação do Grafana, tal como ocorreu com o Prometheus, são exibidas algumas instruções sobre como acessar a aplicação. Essas instruções podem ser visualizadas a qualquer momento usando os comandos:

```bash
helm status meu-grafana -n NAMESPACE
helm status meu-prometheus -n NAMESPACE
```

Liste as aplicações instaladas pelo Helm em todos os namespaces:

```bash
helm list --all-namespaces
```

Observe que a coluna **REVISION** mostra a versão/revisão para cada aplicação instalada.

Remova a aplicação ``meu-prometheus``:

```bash
helm uninstall meu-prometheus --keep-history -n NAMESPACE
```

Liste os pods:

```bash
kubectl get pods -n NAMESPACE
```

O Prometheus foi removido, certo?

Liste novamente as aplicações instaladas pelo Helm em todos os namespaces:

```bash
helm list -A
```

Faça o rollback da remoção da aplicação ``meu-prometheus``, informando a **revision 1**:

```bash
helm rollback meu-prometheus 1 -n NAMESPACE
```

Liste as aplicações instaladas pelo Helm em todos os namespaces:

```bash
helm list -A
```

Olha o Prometheus de volta! A **revision** do Prometheus foi incrementada para **2**.

A revision sempre é incrementada a cada alteração no deploy de uma aplicação. Essa mesma estratégia de rollback pode ser aplicada quando estiver fazendo uma atualização de uma aplicação.

Visualize o histórico de mudanças da aplicação ``meu-prometheus``:

```bash
helm history meu-prometheus -n NAMESPACE
```

> OBS 1.: Se a aplicação for removida sem a opção `--keep-history`, o histórico será perdido e não será possível fazer rollback.
>
> OBS 2.: O Helm não deixa instalar mais de um chart com o mesmo nome de aplicação, mesmo que tenha sido removido com a opção ``--keep-history``. Agora se a aplicação é removida sem a opção ``--keep-history``, aí é possível reutilizar o nome.

Para testar isso, remova as aplicações ``meu-prometheus`` (sem manter o histórico) e ``meu-grafana`` (mantendo o histórico) com os seguites comandos:

```bash
helm uninstall meu-grafana --keep-history -n NAMESPACE
helm uninstall meu-prometheus -n NAMESPACE
```

Visualize o histórico de mudanças da aplicação ``meu-prometheus``:

```bash
helm history meu-prometheus -n NAMESPACE
```

Visualize o histórico de mudanças da aplicação ``meu-grafana``:

```bash
helm history meu-grafana -n NAMESPACE
```

Tente instalar o prometheus novamente:

```bash
helm install meu-prometheus --version=11.12.0 stable/prometheus -n NAMESPACE
```

Tente instalar o Grafana novamente:

```bash
helm install meu-grafana --keep-history --version=5.5.5 stable/grafana -n NAMESPACE
```

Deu erro não foi? Como dito anteriormente, utilizando a opção ``--keep-history`` ao desinstalar a aplicação, o Helm ainda mantém o nome reservado, isso é excelente para rollback.

Agora remova as aplicações sem manter o histórico:

```bash
helm uninstall meu-grafana -n NAMESPACE
helm uninstall meu-prometheus -n NAMESPACE
```

## Criando um Helm Chart

Documentação para criar um Helm Chart:

* https://helm.sh/docs/intro/using_helm/#creating-your-own-charts
* https://helm.sh/docs/topics/charts
* https://helm.sh/docs/chart_template_guide/getting_started
* https://helm.sh/docs/chart_best_practices/
